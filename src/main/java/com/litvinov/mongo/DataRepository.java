package com.litvinov.mongo;

import com.litvinov.model.Data;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface DataRepository extends MongoRepository<Data, ObjectId> {

    List<Data> findByCountry(String country);
    List<Data> findByCity(String city);
    Data findByCoordinates(Double latitude, Double longitude);
}
