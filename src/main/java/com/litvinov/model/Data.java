package com.litvinov.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.TimeZone;

import static com.litvinov.utils.MongoUtil.DATA_COLLECTION_NAME;

@Document(collection = DATA_COLLECTION_NAME)
public class Data {

    @Id
    private ObjectId id;

    private Coordinates coordinates;

    @Field("area_info")
    private AreaInfo areaInfo;

    private LocalDateTime timestamp;

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
    }

    public AreaInfo getAreaInfo() {
        return areaInfo;
    }

    public void setAreaInfo(AreaInfo areaInfo) {
        this.areaInfo = areaInfo;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }
}
