package com.litvinov.model;

import org.springframework.data.mongodb.core.mapping.Field;

import java.time.ZoneId;

public class AreaInfo {

    private String country;
    private String city;
    @Field("zone_id")
    private ZoneId zoneId;
    @Field("area_size")
    private double areaSize;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public ZoneId getZoneId() {
        return zoneId;
    }

    public void setZoneId(ZoneId zoneId) {
        this.zoneId = zoneId;
    }

    public double getAreaSize() {
        return areaSize;
    }

    public void setAreaSize(double areaSize) {
        this.areaSize = areaSize;
    }
}
